<?php
/**
 * @file
 * SodisSearchService implementation.
 */

namespace publicplan\wss\lfs;

use publicplan\wss\base\SearchService;

class Service extends SearchService {

  /**
   * Unique WSS identifier.
   */
  const NAME = 'lfs';

  /**
   * Search/Result facet: Competency.
   */
  const PARAM__COMPETENCY = 'competency';

  /**
   * Search/Result facet: Context.
   */
  const PARAM__CONTEXT = 'context';

  /**
   * Search/Result facet: Copyright.
   */
  const PARAM__COPYRIGHT = 'copyright';

  /**
   * Search/Result facet: Discipline.
   */
  const PARAM__DISCIPLINE = 'discipline';

  /**
   * Search/Result facet: Keyword.
   */
  const PARAM__KEYWORD = 'keyword';

  /**
   * Search/Result facet: Language.
   */
  const PARAM__LANGUAGE = 'language';

  /**
   * Search/Result facet: Resource type.
   */
  const PARAM__RESOURCE_TYPE = 'learningResourceType';

  /**
   * Search/Result facet: Provider.
   */
  const PARAM__PROVIDER = 'provider';

  /**
   * Search/Result facet: Publisher.
   */
  const PARAM__PUBLISHER = 'publisher';

  /**
   * LfS specific parameters.
   */
  const PARAM__TRAEGER = 'Traeger';
  const PARAM__VERANSTALTER = 'Veranstalter';
  const PARAM__VERANSTALTUNGSFORM = 'Veranstaltungsform';

  /**
   * Optional result offset.
   */
  const PARAM__FROM = 'from';

  /**
   * Optional result count restriction.
   */
  const PARAM__SIZE = 'size';

  /**
   * Optional result sorting.
   */
  const PARAM__SORT = 'sort';

  /**
   * Optional result sort order (defaults to self::ORDER_DESC)
   */
  const PARAM__ORDER = 'order';

  /**
   * Sort order: Ascending.
   */
  const ORDER_ASC = 'asc';

  /**
   * Sort order: Descending.
   */
  const ORDER_DESC = 'desc';

  /**
   * Configuration variable names.
   */
  const SETTING__ACTIVATED = 'wss_lfs_settings__search';
//  const SETTING__ACCESS_TOKEN = 'wss_lfs_settings__access_token';
  const SETTING__BASE_URI = 'wss_lfs_settings__base_uri';

  /**
   * Associative array containing setting identifiers and default values.
   *
   * Array keys are internally used with Drupal's variable_*() functions.
   *
   * @return array
   */
  public static function getDefaultSettings() {
    return array(
      self::SETTING__ACTIVATED => NULL,
//      self::SETTING__ACCESS_TOKEN => NULL,
      self::SETTING__BASE_URI => 'http://localhost:9876',
    );
  }

  /**
   * Return the response of a SODIS facet request.
   *
   * @param $facet
   *   Facet for which to request possible values.
   *
   * @return string|FALSE
   *   The raw response body or FALSE on failure.
   *
   */
  public static function getFacetValues($facet) {
    // @todo Implement!
  }
}
