<?php
/**
 * @file
 * This file contains the LfS request class.
 */

namespace publicplan\wss\lfs;

use publicplan\wss\base\SearchRequest;

class Request extends SearchRequest {
  /**
   * Define cache table for requests of this kind.
   */
  const CACHE = 'cache_wss_lfs';

  /**
   * Identifier for this request class.
   *
   * This identifier is used for some inherited generic methods and to determine
   * the corresponding service class.
   *
   * @var string
   */
  protected $service = Service::NAME;

  /**
   * Request method (eg. GET/POST/PUT).
   *
   * @var string
   */
  protected $method = self::METHOD__GET;

  /**
   * List of filters to be requested.
   *
   * @var array
   */
  protected $filters = array(
    'Traeger',
    'Veranstalter',
    'Veranstaltungsform',
  );

  /**
   * Send request.
   *
   * @param bool $use_cache
   *   Whether to use the cache.
   * @param bool $parse_response
   *   Whether to parse the response with the corresponding Response class.
   *
   * @return \publicplan\wss\base\SearchRequest|\stdClass
   *   The response.
   */
  public function send($use_cache = TRUE, $parse_response = TRUE) {
    $glue = strstr($this->getUrl(), '?') ? '&' : '?';
    $this->setUrl($this->getUrl() . $glue . $this->formatQueryString());
    return parent::send($use_cache, $parse_response);
  }

  /**
   * SODIS specific parameter mapping.
   *
   * @return string
   *   Returns given parameters in URL query syntax (without leading '?').
   */
  public function formatQueryString() {
    $query_components = array();

    $params = array_merge(array('filters' => $this->filters), $this->parameters);
    foreach ($params as $key => $value) {
      if (is_array($value)) {
        $value = \array_filter($value, array($this, 'filterEmptyStrings'));
      }
      if (empty($value)) {
        continue;
      }

      $query_components[] = $this->urlEncodeComponent($key, $value);
    }

    return implode('&', $query_components);
  }

  /**
   * Return the acutal parameters array.
   *
   * @return array
   *   The current parameters array.
   */
  public function getParameters() {
    return $this->parameters;
  }

  /**
   * Array filter callback removing empty string.
   *
   * @param mixed $value
   *   Input value.
   *
   * @return bool
   *   TRUE for non-empty values.
   */
  private function filterEmptyStrings($value) {
    return !empty($value);
  }
}
