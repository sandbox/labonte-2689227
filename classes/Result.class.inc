<?php
/**
 * @file
 * This file extends the SearchResultBase class to a SODIS specific form.
 */

namespace publicplan\wss\lfs;

use publicplan\wss\base\SearchResult;

class Result extends SearchResult {

  /**
   * Service identifier.
   *
   * @var string
   */
  protected $service = Service::NAME;

  /**
   * Parse result data.
   *
   * @param array $raw
   *   Optional raw data array. If omitted, $this->raw will be used.
   *
   * @return \publicplan\wss\lfs\Result
   *   Returns iteself.
   */
  public function parse($raw = NULL) {
    if (!isset($raw)) {
      $raw = $this->raw;
    }
    else {
      $this->raw = $raw;
    }

//    $this->data = $this->raw['Event'];
//    $this['id'] = $this->raw['id'];
//    if (!empty($this['AnbieterId']) && !empty($this['Kursnummer'])) {
//      $this['lfs_url'] = sprintf(
//        'http://www.suche.lehrerfortbildung.schulministerium.nrw.de/search/detailedSearch?aid=%s&sid=%s',
//        $this['AnbieterId'],
//        $this['Kursnummer']
//      );
//    }
    $this->data = $this->raw;
    $this['id'] = $this['AnbieterId'] . '--' . $this['Kursnummer'];
    $this['lfs_url'] = $this['Link'];
    if (!empty($this['WWW'])) {
      $this['link'] = preg_match('/^http/', $this['WWW']) ?
        $this['WWW'] : ('http://' . $this['WWW']);
    }
    if (!empty($this['Homepage'])) {
      $this['link2'] = preg_match('/^http/', $this['Homepage']) ?
        $this['Homepage'] : ('http://' . $this['Homepage']);
    }

    return $this;
  }

  /**
   * Return the resource location/URI as string.
   *
   * @return string
   *   Provider URL of for this result/resource.
   */
  public function getResourceLocation($resource_id = NULL) {
    return ''; //@todo Implement!
  }

  /**
   * Returns the service identifier.
   *
   * @return string
   *   LfS event ID.
   */
  public function getServiceId() {
    return $this->raw['id'];
  }

  /**
   * Returns the provider ID.
   *
   * @return string
   *   LfS event ID.
   */
  public function getProviderId() {
    return $this['AnbieterId'];
  }
}
