<?php
/**
 * @file
 * This file contains the LfS specific implementation of the \SearchResponse
 * interface.
 */

namespace publicplan\wss\lfs;

use publicplan\wss\base\SearchResponse;

class Response extends SearchResponse {
  /**
   * Appropriate service name.
   *
   * @var string
   */
  protected $service = Service::NAME;

  /**
   * Parse common response data.
   *
   * @return \publicplan\wss\sodis\Response
   *   Returns itself.
   */
  protected function parseResponseData() {
    $this->raw = json_decode($this->body, TRUE);
    $this->data['total'] = (int) $this->raw['total'];
    $this->data['limit'] = (int) $this->raw['limit'];
    $this->data['offset'] = (int) $this->raw['offset'];
    $this->data['count'] = (int) $this->raw['count'];

    if (empty($this->raw['filters'])) {
      return $this;
    }

    foreach ($this->raw['filters'] as $filter_name => $filter) {
      if (empty($filter)) {
        continue;
      }
      $this->data['filters'][$filter_name] = array();
      foreach ($filter as $filter_value => $filter_value_count) {
        $this->data['filters'][$filter_name][$filter_value] = $filter_value_count;
      }
      arsort($this->data['filters'][$filter_name], SORT_NUMERIC);
    }

    return $this;
  }

  /**
   * Return array of SODIS results.
   *
   * @return array
   *   Array of SODIS result arrays (hits).
   */
  protected function extractResults() {
    if (!isset($this->raw['events']) ||
        !is_array($this->raw['events'])) {
      return array();
    }

    return $this->raw['events'];
  }
}
