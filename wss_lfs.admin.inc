<?php
/**
 * @file
 * Module's admin settings.
 */

use publicplan\wss\lfs\Service;

/**
 * Callback function for SODIS specific administration settings.
 *
 * @return array Renderable form.
 */
function wss_lfs_admin_settings($form_id, $form_state) {
  $settings = array();

  $settings[Service::SETTING__ACTIVATED] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate LfS search service'),
    '#default_value' => Service::getSetting(Service::SETTING__ACTIVATED),
    '#weight' => 10,
  );

  $settings[Service::SETTING__BASE_URI] = array(
    '#type' => 'textfield',
    '#title' => t('LfS service address'),
    '#description' => t('Webservice base uri with protocol and trailing slash.'),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => Service::getSetting(Service::SETTING__BASE_URI),
    '#weight' => 20,
  );

  return system_settings_form($settings);
}
